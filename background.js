  
browser.contextMenus.create({
  id: "copy-link-to-clipboard",
  title: "Copy link to clipboard",
  contexts: ["link"],
});
browser.contextMenus.create({
  id: "clickme",
  title: "Click me!",
  contexts: ["all"]
});

browser.contextMenus.create({
  id: "clickme-separator-1",
  type: "separator",
  contexts: ["all"]
});

browser.contextMenus.create({
  id: "clickme-radio-1",
  type: "radio",
  title: "Choose me - radio 1!",
  contexts: ["all"],
  checked: true
});

browser.contextMenus.create({
  id: "clickme-radio-2",
  type: "radio",
  title: "Choose me - radio 2!",
  contexts: ["all"],
  checked: false
});

browser.contextMenus.create({
  id: "clickme-separator-2",
  type: "separator",
  contexts: ["all"]
});

browser.contextMenus.create({
  id: "clickme-checkbox",
  type: "checkbox",
  title: "Uncheck me: checkbox!",
  contexts: ["all"],
  checked: true
});

/*
Open a new tab, and load "my-page.html" into it.
*/
function openMyPage() {
  console.log("injecting");

  // browser.tabs.create({
  //   "url": "http://google.com.vn"
  // });


  // var executing = browser.tabs.executeScript({
  //   file: "/borderify.js",
  //   allFrames: true
  // });

  // var makeItGreen = "document.getElementsByTagName('video')[0].play(); ";
  // if (pauseFlg){
  //   makeItGreen = "document.getElementsByTagName('video')[0].pause(); ";
  // }
  // pauseFlg = !pauseFlg;

  // var executing = browser.tabs.executeScript({
  //   code: makeItGreen
  // });

  var executing = browser.tabs.executeScript({
    file: "/content-script.js",
    allFrames: true
  });

  executing.then(onExecuted, onError);

  // var gettingCurrent = browser.tabs.getCurrent();
  // gettingCurrent.then(function onGot(tabInfo) {
  //   console.log(tabInfo);
  // }, function onError(error) {
  //   console.log(`Error: ${error}`);
  // });

}


var pauseFlg = false;

function onExecuted(result) {
  console.log(`We made it green`);
}

function onError(error) {
  console.log(`Error: ${error}`);
}

/*
Add openMyPage() as a listener to clicks on the browser action.
*/
browser.browserAction.onClicked.addListener(openMyPage);